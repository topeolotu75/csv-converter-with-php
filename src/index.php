<?php

// Read input CSV file
$inputFilePath = __DIR__ . '/input/input.csv';
$outputFilePath = __DIR__ . '/output/output.csv';

if (!file_exists($inputFilePath)) {
    die("Input file does not exist.");
}

$csvData = array_map('str_getcsv', file($inputFilePath));

// Remove header if present
$header = array_shift($csvData);

// Sort CSV data by dates
usort($csvData, function($a, $b) {
    return strtotime($a[0]) - strtotime($b[0]);
});

// Calculate total for each date
$totals = array();
foreach ($csvData as $row) {
    $date = $row[0];
    $amount = floatval($row[1]);
    if (isset($totals[$date])) {
        $totals[$date] += $amount;
    } else {
        $totals[$date] = $amount;
    }
}

// Write output to CSV file
$outputFile = fopen($outputFilePath, 'w');
fputcsv($outputFile, array('Date', 'Total'));

foreach ($totals as $date => $total) {
    fputcsv($outputFile, array($date, $total));
}

fclose($outputFile);

echo "Output written to: $outputFilePath\n";

